## Assignment Instructions
Before you start you need to have a Bitbucket account, you can create an account here:
https://bitbucket.org

To fork the project click on the fork menu on left side. When you fork the project you should use your own unique project name and do not make the repository private, so that we can view the project later.

To fork the project click on the “Fork” link in menu on the left side. Give your project a unique name and don’t make the repository private, so that we can pull repository for review.

## Coolstuff - Frontend Developer Assignment 
 

### Objective ###
The Goal of this assignment is  to test your Angular 2, HTML skills. Successful completion will prove your level of coding ability in Angular 2 as well as Unit testing skills. Code quality is equally important as functionality.  
 
**Introduction** 

The task is to create a small Angular 2 application. Any version of Angular 2 is accepted. Use Unit test in karma or any other framework  to test the quality of the written code functions. 

**Task 1**: 

Create a layout in HTML,CSS to display two (2) products similar to any E-commerce product page. You are allowed to use your own layout. Title of the product ,product Image, price does not matter at this point. 
Sample layout example : 

![Screen Shot 2017-04-18 at 16.28.00.png](https://bitbucket.org/repo/x888L5r/images/3799344757-Screen%20Shot%202017-04-18%20at%2016.28.00.png)

**Task 2** : 
To recive a valid token, you have to make a POST request towards our API gateway. This POST request also requires a body data parameter as JSON object. 

**URL** : https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/ 

**Request type** : POST
  
**Use this body parameter on request** : 
{ "country_code" :"de" } 

After successful HTTP call you will receive a JSON object containing two property’s, token and country_code. An example of a sample response:
![token.PNG](https://bitbucket.org/repo/x888L5r/images/317444897-token.PNG)

Use your local browser storage for storing token. This token acts as identification for particular cart. You are allowed to use any name to store the variables at your convenience.  
 
**Task 3** : 

Now when you have you great layout working, it is time to add functionality to it.  
Create a function to add products to your cart, by pressing the BUY button, it will execute a PUT request to add item to the respective cart. Read the token stored in local storage which you created in previous task. Products should have different unique article id (88 and 102). 

**URL** : https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/carts/ (your encrypted cart token from TASK 2) 

**Sample request URL** : https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/carts/eJwNyTuWAjAIAMC7pLbgmxBvAwR8NhbqVvu8u7Yz_5la5h_p1u5N1vz38_5la5hfescR1RZ5oylpDZnOFIVT8La0iLxRriXoe2KnqRHrZfUwivks7pgLtyLmvcBtBrS6w_p1u5J_p1u5Okmjs7eie0AmNyB2gfoAheshN9XEb6830_5la5h44qySQGNWFjm0kmfLyePMP8 

**Request type** : PUT  


**Use below body parameter on request** : 

{ 
   "articles": [ 
       {   "article_id": 88, 
           "article_quantity": 1 
       } 
   ], 
   "language_code": "de", 
   "currency_code": "EUR", 
   "country_code":"de" 
} 

After successful request you will get a JSON object response with a property called cart. Print that cart response anywhere on the HTML page.Use your creativity to present the response nicely.

For any technical question regarding the test please contact [shamrat.asfaq@coolstuff.com](Link URL) 

After completion of the assignment you can send us the link to your repository on this email:
**workit@coolstuff.com**
## Angular CLI

## Prerequisites

Both the CLI and generated project have dependencies that require Node 6.9.0 or higher, together
with NPM 3 or higher.

## Installation

**BEFORE YOU INSTALL:** please read the [prerequisites](#prerequisites)
```bash
npm install -g @angular/cli
```

## Usage

```bash
ng help
```

### Generating and serving an Angular project via a development server

```bash
ng new PROJECT_NAME
cd PROJECT_NAME
ng serve
```
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

You can configure the default HTTP host and port used by the development server with two command-line options :

```bash
ng serve --host 0.0.0.0 --port 4201
```

### Generating Components, Directives, Pipes and Services

You can use the `ng generate` (or just `ng g`) command to generate Angular components:

```bash
ng generate component my-new-component
ng g component my-new-component # using the alias

# components support relative path generation
# if in the directory src/app/feature/ and you run
ng g component new-cmp
# your component will be generated in src/app/feature/new-cmp
# but if you were to run
ng g component ../newer-cmp
# your component will be generated in src/app/newer-cmp
```

## Documentation **Optional**

The documentation for the Angular CLI is located in this repo's [wiki](https://github.com/angular/angular-cli/wiki).